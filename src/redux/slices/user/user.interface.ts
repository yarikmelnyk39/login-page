import { UserType } from 'redux/api/auth/auth.interface';

export type UserState = {
  user?: UserType;
  accessToken?: string;
};
