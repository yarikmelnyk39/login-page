import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AuthResponseType } from 'redux/api/auth/auth.interface';

import { AUTH_TOKEN } from 'modules/login/utils';
import { authApi } from 'redux/api/auth/auth';
import { UserState } from './user.interface';

export const userSlice = createSlice({
  initialState: {
    accessToken: localStorage.getItem(AUTH_TOKEN),
  } as UserState,
  name: 'userSlice',
  reducers: {
    setUser: (state, action: PayloadAction<AuthResponseType>) => {
      state.user = action.payload.user;
      state.accessToken = action.payload.accessToken;
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      authApi.endpoints.login.matchFulfilled,
      (state, { payload }) => {
        state.accessToken = payload.accessToken;
        state.user = payload.user;
        localStorage.setItem(AUTH_TOKEN, payload.accessToken);
      }
    );
  },
});

export default userSlice.reducer;

export const { setUser } = userSlice.actions;
