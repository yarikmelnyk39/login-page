import { authApi } from 'redux/api/auth/auth';
import userReducer from 'redux/slices/user/userSlice';
import { notificationsApi } from './api/notifications/notifications';

const rootReducer = {
  [authApi.reducerPath]: authApi.reducer,
  [notificationsApi.reducerPath]: notificationsApi.reducer,
  user: userReducer,
};

export default rootReducer;
