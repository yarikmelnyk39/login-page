import { createApi } from '@reduxjs/toolkit/query/react';
import baseQuery from 'helpers/baseQuery';
import { INotification } from './notifications.interface';

export const notificationsApi = createApi({
  reducerPath: 'notificationsApi',
  baseQuery: baseQuery('/'),
  endpoints: (builder) => ({
    getNotifications: builder.query<INotification[], void>({
      query: () => 'notifications',
    }),
  }),
});

export const { useGetNotificationsQuery } = notificationsApi;
