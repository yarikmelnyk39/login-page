export const APP_SETTINGS = {
  API: {
    MOCKUP_API: process.env.REACT_APP_JSON_SERVER_API_URL || '/api',
  },
};

export enum APP_ROUTES {
  LOGIN = '/login',
  HOME = '/',
  INTRO = 'intro', 
}
