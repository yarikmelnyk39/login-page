import { Route } from 'app/app.interface';
import { RequireAuth } from 'components/AuthProtected';
import AuthLayout from 'layouts/authLayout/AuthLayout';
import DashboardLayout from 'layouts/dashboardLayout/DashboardLayout';
import Dashboard from 'modules/dashboard/Dashboard';
import Error404 from 'modules/error404/Error404';
import Intro from 'modules/intro/Intro';
import Login from 'modules/login/Login';
import { APP_ROUTES } from './utils';

export const routes: Route[] = [
  {
    path: APP_ROUTES.LOGIN,
    element: <AuthLayout />,
    children: [
      {
        path: '',
        element: <Login />,
      },
    ],
  },
  {
    path: APP_ROUTES.HOME,
    element: <DashboardLayout />,
    children: [
      {
        path: '',
        element: (
          <RequireAuth>
            <Dashboard />
          </RequireAuth>
        ),
      },
      {
        path: APP_ROUTES.INTRO,
        element: <Intro />,
      },
    ],
  },
  {
    path: '*',
    element: <Error404 />,
  },
];
