import Notifications from 'components/Notifications';
import React from 'react';

const Dashboard: React.FC = () => {
  return <Notifications max={1} />;
};

export default Dashboard;
