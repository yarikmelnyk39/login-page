import { Box, CardMedia } from '@mui/material';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';
import cybellumSign from 'assets/images/login/cybellum-sign.svg';
import monitor from 'assets/images/login/imac-dig-twins.png';
import { Controller, useForm } from 'react-hook-form';
import { Navigate } from 'react-router';
import { useLoginMutation } from 'redux/api/auth/auth';
import Styled from './login.styled';
import { ERRORS, NAV_URLS } from './utils';
import { APP_ROUTES } from 'app/utils';

const isFetchBaseQueryError = (error: any): error is FetchBaseQueryError => {
  return (error as FetchBaseQueryError).status !== undefined;
};

interface ILoginFormInputs {
  email: string;
  password: string;
}

export default function Login() {
  const [login, { error, isSuccess }] = useLoginMutation();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<ILoginFormInputs>();

  if (isSuccess) {
    return <Navigate to={APP_ROUTES.HOME} />;
  }

  return (
    <Styled.Wrapper>
      <Styled.Form>
        <CardMedia
          component='img'
          alt='Cybellum'
          image={cybellumSign}
          sx={{ width: 150 }}
        />
        <Styled.Title variant='h1'>
          Welcome to the Product Security Platform
        </Styled.Title>

        <Controller
          name='email'
          control={control}
          defaultValue=''
          rules={{ required: ERRORS.email }}
          render={({ field }) => (
            <Styled.Input
              {...field}
              label='Username'
              error={!!error || !!errors.email}
              helperText={errors.email?.message}
            />
          )}
        />

        <Controller
          name='password'
          control={control}
          defaultValue=''
          rules={{ required: ERRORS.password }}
          render={({ field }) => (
            <Styled.Input
              {...field}
              label='Password'
              type='password'
              sx={{ mt: 3 }}
              error={!!error || !!errors.password}
              helperText={
                error && isFetchBaseQueryError(error)
                  ? '' + error.data
                  : errors.password?.message
              }
            />
          )}
        />

        <Styled.ForgotPasswordBtn>
          Forgot your password?
        </Styled.ForgotPasswordBtn>

        <Styled.LoginBtn
          variant='contained'
          type='submit'
          onClick={handleSubmit(login)}
        >
          Log in
        </Styled.LoginBtn>

        <Styled.LinksWrapper>
          {Object.keys(NAV_URLS).map((link) => (
            <Styled.Link key={link}>{link}</Styled.Link>
          ))}
        </Styled.LinksWrapper>
      </Styled.Form>

      <Box sx={{ width: '66%' }}>
        <CardMedia component='img' alt='Digital Twins' image={monitor} />
      </Box>
    </Styled.Wrapper>
  );
}
