export const AUTH_TOKEN = 'authToken';

export const ERRORS = {
  email: 'Username is required',
  password: 'Password is required',
  login: "The email or password you've entered don't match",
};

export const NAV_URLS = {
  'Privacy policy': '/privacy-policy',
  'Terms of use': '/terms-of-use',
  'Contact us': '/contact-us',
};

export const setAuthTokens = (token: string) => {
  localStorage.setItem(AUTH_TOKEN, token);
};
export const removeAuthTokens = () => {
  localStorage.removeItem(AUTH_TOKEN);
};
