import { Button, TextField, Typography } from '@mui/material';
import BoxMui from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import errorIcon from '../../assets/images/login/error.svg';

const Wrapper = styled(BoxMui)(() => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  marginLeft: '180px',
}));

const Form = styled(BoxMui)(() => ({
  width: '636px',
  display: 'flex',
  flexDirection: 'column',
  marginBottom: '75px',
}));

const Title = styled(Typography)(() => ({
  marginTop: '32px',
  marginBottom: '32px',
  fontSize: '56px',
  textTransform: 'capitalize',
  letterSpacing: '-0.5px',
  lineHeight: '64px',
}));

const Input = styled(TextField)(({ error, theme }) => {
  return {
    height: '68px',
    width: '400px',
    '::after': error && {
      content: '""',
      position: 'absolute',
      right: '12px',
      top: '32px',
      background: `url(${errorIcon}) no-repeat`,
      height: '24px',
      width: '24px',
    },
    '& .MuiInputLabel-root': {
      marginLeft: '16px',
      fontSize: '14px',
    },
    '& input': {
      height: '48px',
      boxSizing: 'border-box',
    },
    '& p.Mui-error': {
      position: 'absolute',
      bottom: '-20px',
      left: '-10px',
    },
  };
});

const ForgotPasswordBtn = styled(Button)(({ theme }) => {
  return {
    color: theme.palette.primary.main,
    width: '170px',
    marginTop: '16px',
    paddingLeft: 0,
  };
});

const LoginBtn = styled(Button)(({ theme }) => {
  return {
    marginTop: '16px',
    marginBottom: '32px',
    clear: 'both',
    width: '400px',
    height: '40px',
    backgroundColor: theme.palette.secondary.main,
    color: 'black',
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
  };
});

const LinksWrapper = styled(BoxMui)(() => {
  return {
    position: 'absolute',
    bottom: '60px',
    display: 'flex',
    width: '400px',
    justifyContent: 'space-between',
    alignItems: 'center',
  };
});

const Link = styled(Button)(({ theme }) => {
  return {
    color: theme.palette.primary.main,
    padding: '4px 8px',
    width: '170px',
    fontSize: '16px',
    fontWeight: 500,
  };
});

const styledComponents = {
  Wrapper,
  Input,
  Form,
  Title,
  ForgotPasswordBtn,
  LoginBtn,
  LinksWrapper,
  Link,
};

export default styledComponents;
