import React from 'react';
import { render, fireEvent, waitFor, screen, act } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { configureStore } from '@reduxjs/toolkit';
import userReducer from 'redux/slices/user/userSlice';
import { authApi } from 'redux/api/auth/auth';
import Login from '../Login';
import { AUTH_TOKEN } from '../utils';

const renderWithProviders = (component: React.ReactElement) => {
  const store = configureStore({
    reducer: {
      [authApi.reducerPath]: authApi.reducer,
      user: userReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(authApi.middleware),
  });

  return render(
    <Provider store={store}>
      <BrowserRouter>
        {component}
      </BrowserRouter>
    </Provider>
  );
};

describe('<Login />', () => {
  test('renders the login form', () => {
    renderWithProviders(<Login />);
    expect(screen.getByLabelText(/username/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/password/i)).toBeInTheDocument();
  });

  test('allows the user to enter credentials', async () => {
    renderWithProviders(<Login />);
    await act(async () => {
      fireEvent.change(screen.getByLabelText(/username/i), { target: { value: 'test@example.com' } });
      fireEvent.change(screen.getByLabelText(/password/i), { target: { value: 'password123' } });
      fireEvent.click(screen.getByRole('button', { name: /log in/i }));
    });
    expect(screen.getByLabelText(/username/i)).toHaveValue('test@example.com');
    expect(screen.getByLabelText(/password/i)).toHaveValue('password123');
  });

  test('displays an error message for failed login', async () => {  
    renderWithProviders(<Login />);
    await act(async () => {
      fireEvent.change(screen.getByLabelText(/username/i), { target: { value: 'test@example.com' } });
      fireEvent.change(screen.getByLabelText(/password/i), { target: { value: 'password123' } });
      fireEvent.click(screen.getByRole('button', { name: /log in/i }));
    });
  
    await waitFor(() => {
      expect(screen.getByText(/Cannot find user/i)).toBeInTheDocument();
    });
  });

  test('no error message on right credentials input', async () => {
    renderWithProviders(<Login />);
    await act(async () => {
      fireEvent.change(screen.getByLabelText(/username/i), { target: { value: 'user@email.com' } });
      fireEvent.change(screen.getByLabelText(/password/i), { target: { value: 'UserPassword' } });
      fireEvent.click(screen.getByRole('button', { name: /log in/i }));
    });

    await waitFor(() => {
      expect(screen.queryByText(/Cannot find user/i)).not.toBeInTheDocument();
    });
  });

  test('stores accesstoken on successful login', async () => {
    renderWithProviders(<Login />);
    await act(async () => {
      fireEvent.change(screen.getByLabelText(/username/i), { target: { value: 'user@email.com' } });
      fireEvent.change(screen.getByLabelText(/password/i), { target: { value: 'UserPassword' } });
      fireEvent.click(screen.getByRole('button', { name: /log in/i }));
    });

    await waitFor(() => {
      expect(localStorage.getItem(AUTH_TOKEN)?.length).toBeGreaterThan(10);
    });
  });

  test('redirects to home page on successful login', async () => {
    window.history.pushState({}, 'Login Page', '/login');
    renderWithProviders(<Login />);
    await act(async () => {
      fireEvent.change(screen.getByLabelText(/username/i), { target: { value: 'user@email.com' } });
      fireEvent.change(screen.getByLabelText(/password/i), { target: { value: 'UserPassword' } });
      fireEvent.click(screen.getByRole('button', { name: /log in/i }));
    });

    await waitFor(() => {
      expect(window.location.pathname).toBe('/');
    });
  });
});
