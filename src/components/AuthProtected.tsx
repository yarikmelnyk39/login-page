import { APP_ROUTES } from 'app/utils';
import { AUTH_TOKEN } from 'modules/login/utils';
import { Navigate, useLocation } from 'react-router-dom';

const isAuthenticated = () => {
  return !!localStorage.getItem(AUTH_TOKEN);
};

export const RequireAuth = ({ children }: { children: any }) => {
  const location = useLocation();

  if (!isAuthenticated()) {
    return <Navigate to={APP_ROUTES.LOGIN} state={{ from: location }} replace />;
  }

  return children;
};
