import { Alert } from '@mui/material';
import { APP_ROUTES } from 'app/utils';
import { isBaseQueryErrorWithStatus } from 'helpers/baseQuery';
import { Navigate } from 'react-router';
import { useGetNotificationsQuery } from 'redux/api/notifications/notifications';
import { INotification } from 'redux/api/notifications/notifications.interface';

const Notifications = ({ max }: { max: number }) => {
  const { data: notifications, error, isLoading } = useGetNotificationsQuery();

  if (isLoading) return <div>Loading...</div>;
  if (error) {
    if (isBaseQueryErrorWithStatus(error) && error.status === 401) {
      return <Navigate to={APP_ROUTES.LOGIN} />;
    }
    return (
      <Alert sx={{ width: '100vw' }}>
        <p>Error occurred!</p>
      </Alert>
    );
  }

  return (
    <>
      {notifications?.slice(0, max)?.map((notification: INotification) => (
        <Alert sx={{ width: '100vw' }} key={notification.id}>
          <h3>{notification.title}</h3>
          <p>{notification.description}</p>
          <small>Created: {notification.created}</small>
        </Alert>
      ))}
    </>
  );
};

export default Notifications;
